class CreateFriend < ActiveRecord::Migration[6.0]
  def change
    create_table :friends do |t|
	  t.integer :owner_id
	  t.integer :friend_id
	  t.integer :status, null: false, default: 0
    end
  end
end
