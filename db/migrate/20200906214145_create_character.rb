class CreateCharacter < ActiveRecord::Migration[6.0]
  def change
    create_table :characters do |t|
	   t.string :name, null: false
       t.string :title
	   t.jsonb :stats
	   t.jsonb :skills
	   t.boolean :is_unique, default: false
	   t.boolean :is_player, default: true
	   t.integer :state, default: 0
	   t.references :user
	   t.references :game
    end
  end
end
