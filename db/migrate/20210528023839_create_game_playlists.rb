class CreateGamePlaylists < ActiveRecord::Migration[6.0]
  def change
    create_table :game_playlists do |t|
      t.integer :game_id
      t.integer :playlist_id

      t.timestamps
    end
  end
end
