class CreateScene < ActiveRecord::Migration[6.0]
  def change
    create_table :scenes do |t|
      t.string :name
      t.text :description
	  t.integer :category
	  t.integer :game_id
    end
  end
end
