class CreateGames < ActiveRecord::Migration[6.0]
  def change
    create_table :games do |t|
	  t.string :name
	  t.integer :module
	  t.references :current_scene, null: true
	  t.references :owner
	  t.timestamps
	end
  end
end
