class CreateJukeboxes < ActiveRecord::Migration[6.0]
  def change
    create_table :jukeboxes do |t|
      t.integer :playlist_id
      t.integer :current_song, null: false, default: 1
      t.integer :game_id
      t.boolean :is_paused, null: false, default: false

      t.timestamps
    end
  end
end
