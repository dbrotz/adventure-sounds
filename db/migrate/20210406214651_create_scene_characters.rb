class CreateSceneCharacters < ActiveRecord::Migration[6.0]
  def change
    create_table :scene_characters do |t|
      t.references :character, null: false, foreign_key: true
      t.references :scene, null: false, foreign_key: true

      t.timestamps
    end
  end
end
