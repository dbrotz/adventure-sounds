class CreateGameInvitation < ActiveRecord::Migration[6.0]
  def change
    create_table :game_invitations do |t|
	  t.references :game
	  t.references :user
	  t.integer :status
    end
  end
end
