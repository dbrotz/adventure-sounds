user = User.create!(name: "Afan", password: "Starwars92", email: "drbrotz@protonmail.com")
User.create!(name: "Test1", password: SecureRandom.base64(13), email: "test1@test.com")
User.create!(name: "Test2", password: SecureRandom.base64(13), email: "test2@test.com")
User.create!(name: "Test3", password: SecureRandom.base64(13), email: "test3@test.com")
User.create!(name: "Test4", password: SecureRandom.base64(13), email: "test4@test.com")

Friend.create!(owner: user, friend: User.all[1])
Friend.create!(owner: user, friend: User.all[2])

game = Game.new(owner_id: user.id, name: "Test Game", scenes: [
    Scene.new(name: "Spooky Gas Station", description: "This is a spooky gas station", category: "major"),
    Scene.new(name: "Spooky Cabin", description: "This is a spooky cabin", category: "minor")
  ],
  game_invitations: [
    GameInvitation.new(user: User.find(1), status: :accepted),
    GameInvitation.new(user: User.find(2), status: :accepted)
  ],

  characters: [
    Character.new(name: "Magror", user: user, stats: [
      {"name":"strength","value":15}, 
      {"name":"dexterity","value":11}, 
      {"name":"constitution","value":15},
      {"name":"wisdom","value":16},
      {"name":"intelligence","value":12},
      {"name":"charisma","value":12}
     ]),
    Character.new(name: Faker::Games::Heroes.name, user: User.find(1)),
    Character.new(name: "Spider", user: user, is_player: false, is_unique: false),
    Character.new(name: Faker::Games::Heroes.name, user: User.find(2)),
  ]
)

game.save!

game.scenes[0].image.attach(io: File.open("tmp/spookygasstation.jpg"), filename: "spookygasstation.jpg")
game.scenes[0].audio.attach(io: File.open("tmp/outside.mp3"), filename: "outside.mp3")

game.scenes[1].image.attach(io: File.open("tmp/spookycabin.jpg"), filename: "spookycabin.jpg")
game.scenes[1].audio.attach(io: File.open("tmp/electronic.mp3"), filename: "electronic.mp3")

#game.themes.attach(io: File.open("tmp/outside.mp3"), filename: "outside.mp3")
#game.themes.attach(io: File.open("tmp/electronic.mp3"), filename: "electronic.mp3")

song = Song.new(user: User.first, name: "electronic")
song.audio.attach(io: File.open("tmp/electronic.mp3"), filename: "electronic.mp3")
song.save!

playlist = Playlist.create!(user: user)
playlist.playlist_songs << PlaylistSong.new(order: 1, song: song)

game.playlists << playlist
