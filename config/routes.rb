Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: "root#index"
  resources :games do
    resources :characters, controller: "game_characters"
    resources :npcs
    post 'set_theme/:theme_id', to: 'games#set_theme'
    get 'admin'
    resources :scenes do
      member do
        post 'set', to: 'set'
      end
    end
    resources :playlists, controller: :game_playlists
  end
  resource :home do
    post 'friends'
    post 'accept_friend/:id', to: 'users#accept_friend'
  end
  resources :friends
  resources :characters
  resources :invites, controller: :game_invites
  resources :playlists do
    resources :playlist_songs
  end
  resources :songs
end
