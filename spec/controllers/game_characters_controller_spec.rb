require 'rails_helper'

RSpec.describe GameCharactersController, type: :controller do
  fixtures :users
  fixtures :characters

  describe "POST create" do
    let(:bob) { users(:bob) }
    let(:magror) { characters(:magror) }
	  let(:game) { Game.create!(owner: bob, name: "test") }

    it "create a character" do
      magror.update(user: bob)
      sign_in bob
      expect {
        post :create, params: {game_id: game.id, name: magror.name}
      }.to change{magror.reload.game_id}.to game.id

      expect(response).to have_http_status(302)
    end

    it "fail to create a nameless character" do
      magror.update(user: bob)
      sign_in bob
      post :create, params: {game_id: game.id, name: ""}
      expect(magror.reload.game_id).to be_nil

      expect(response).to have_http_status(302)
    end
  end
end
