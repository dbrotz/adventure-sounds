require 'rails_helper'

RSpec.describe CharactersController, type: :controller do
  fixtures :users

  describe "POST create" do
    let(:bob) { users(:bob) }

    it "create a character" do
      sign_in bob
      expect {
        post :create, params: {character: { name: "magror" } }
      }.to change{Character.count}.by 1

      expect(response).to have_http_status(302)
    end

    it "create a character" do
      sign_in bob
      expect {
        post :create, params: {character: { name: "" } }
      }.to change{Character.count}.by 0
    end
  end
end
