require 'rails_helper'

RSpec.describe GamesController, type: :controller do
  fixtures :users

  describe "GET show" do
    let(:bob) {users(:bob)}

    it 'has no audio_url' do
	    game = Game.create!(owner: bob, name: "test")
	    sign_in bob
	    get :show, params: {id: game.id}

	    expect(response).to have_http_status(200)
	  end
  end

  describe "POST create" do
    let(:bob) {users(:bob)}

    it "creates a game with friends" do
      sign_in bob
      friends = [
        User.create!(email: "friend1@test.com", password: "foobar")
      ]

      expect {
        post :create, params: {players: friends.map{ |f| f.id}, game: { name: "test" }}
      }.to change{GameInvitation.count}.by 1
      expect(response).to have_http_status(302)
    end

    it "fails when adding non friends" do
    
    end
  end
end
