require 'rails_helper'
require 'taglib'

RSpec.describe EncodeService do
  let(:input_filepath) {File.join(Rails.root, "spec", "fixtures", "files", "sample.wav")}
  let(:output_filepath) {File.join(Rails.root, "tmp", "sample.mp3")}
  after(:each) {File.delete(output_filepath) if File.exists?(output_filepath)}

  it 'creates an mp3' do
    described_class.encode(input_filepath)
  end

  it 'creates a tmp file' do
    described_class.encode(input_filepath)
	expect(File).to exist(output_filepath)
  end

  it 'returns nil on bad input' do
    expect{ described_class.encode("") }.to raise_error(EncodeService::EncodeError)
  end

  it 'removes all tags' do
    tagged_filepath = File.join(Rails.root, "tmp", "sample.wav")
	FileUtils.cp(input_filepath, tagged_filepath)

	TagLib::RIFF::WAV::File.open(tagged_filepath) do |file|
      v2_tag = file.id3v2_tag
	  frame = TagLib::ID3v2::UserTextIdentificationFrame.new("TCOP")
	  frame.text_encoding = TagLib::String::UTF8
	  frame.text = "COMMENTS"
	  v2_tag.add_frame(frame)
	  file.save
	end
    
	described_class.encode(input_filepath)

    TagLib::RIFF::WAV::File.open(File.join(Rails.root, "tmp", "sample.mp3")) do |file|
      v2_tag = file.id3v2_tag
	  expect(v2_tag.frame_list("TCOP").first.to_s).to eq('')
	end

    File.delete(tagged_filepath)
  end
end
