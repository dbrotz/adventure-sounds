require 'rails_helper'

RSpec.describe EncodeJob, type: :job do
  include ActiveJob::TestHelper

  fixtures :songs
  let(:song) { songs(:sample) }
  #let(:audio) {File.join(Rails.root, "spec", "fixtures", "files", "sample.wav")}


  it 'updates the song\'s audio after success' do
#	song.audio = fixture_file_upload(audio)
	perform_enqueued_jobs { described_class.perform_later(song) }
	assert_performed_jobs 1
  end

  it 'created an email on failure' do
	song.audio = nil
    subject {  described_class.perform_later(song) }
	perform_enqueued_jobs { subject }
	assert_performed_jobs 1
  end
end
