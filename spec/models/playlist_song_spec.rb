require 'rails_helper'

RSpec.describe PlaylistSong, type: :model do
  fixtures :users

  let(:playlist) { Playlist.create!(user: users(:bob)) }
  describe "swap_order" do
    before(:each) do
      @song = PlaylistSong.create!(playlist: playlist, order: 1, song: Song.create!(user: users(:bob), name: "test"))
    end

    it "swaps with another song of the same playlist" do

      other_song = PlaylistSong.create!(playlist: playlist, order: 2, song: Song.create!(user: users(:bob), name: "test"))
      @song.swap_order(other_song)
      expect(@song.order).to be(2)
      expect(other_song.order).to be(1)
    end
    it "only swaps with songs in the same playlist" do
      bad_playlist = Playlist.create!(user: users(:bob))
      bad_song = PlaylistSong.create!(playlist: bad_playlist, order: 2, song: Song.create!(user: users(:bob), name: "test"))
      @song.swap_order(bad_song)
      expect(@song.order).to be(1)
      expect(bad_song.order).to be(2)
    end 
  end
end
