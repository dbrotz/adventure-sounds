class PlaylistSongBlueprint < Blueprinter::Base
  identifier :id

  fields :order
  field :name do |playlist_song, options|
    playlist_song.song.name
  end
end

