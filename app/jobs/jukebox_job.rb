require 'shout'
require 'open-uri'

class JukeboxJob < ApplicationJob
  def perform(jukebox_id)
    config = Rails.application.config_for(:icecast)
    secret = Rails.application.credentials.config[:icecast]
    s = Shout.new
    jukebox = Jukebox.find(jukebox_id)
    playlist = jukebox.playlist
    songs = playlist.playlist_songs.where(PlaylistSong.arel_table[:order].gteq(jukebox.current_song)).order(:order)
    
    s.mount = "/#{jukebox.id}"
    s.charset = "UTF-8"
    s.host = config[:hostname]
    s.port = config[:port]
    s.user = secret[:username]
    s.pass = secret[:password]
    s.format = Shout::MP3
    s.description = "Game: #{jukebox.game.id} Playlist: #{jukebox.playlist.id}"
    s.connect

    last_check = Time.now.to_f * 1000 + (1000)
    songs.each { |song|
        song.song.audio.open do |file|
#		file.seek(128 * 1024 * 60, IO:SEED_CUR)
        m = ShoutMetadata.new
        m.add 'filename', song.song.name
        s.metadata = m
        time = Time.now.to_f * 1000

		puts "Times are #{time} - #{last_check}"
        while data = file.read(16384) do
          s.send data
          s.sync
        end
        if time >= last_check then
          jukebox = Jukebox.find(jukebox_id)
          if jukebox.is_paused then
            sleep(1)
            jukebox = Jukebox.find(jukebox_id)
          end
          last_check = Time.now.to_f * 1000 + (1000)
        end
        jukebox.update!(current_song: song.order)
      end
    }

    jukebox.destroy
    s.disconnect
  end
end
