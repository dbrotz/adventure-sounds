class EncodeJob < ApplicationJob

  def perform(song)
    EncodeService.new(song.audio.filename)
    rescue EncodeService::EncodeError => e
      EncodingFailureMailer.failure(input_file).deliver_later    
  end
end
