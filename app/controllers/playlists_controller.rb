class PlaylistsController < ApplicationController
  before_action :authenticate_user!

  def new
    @playlist = Playlist.new
  end

  def create
    @playlist = Playlist.new(user: current_user, name: params[:playlist][:name])
    song_list = Song.find(params[:playlist][:songs])
    PlaylistSong.transaction do
      PlaylistSong.create!(song_list.map do |s| {playlist: @playlist, song: s} end)
    end
    if @playlist.save then
      redirect_to request.referrer
    else
      render :new
    end
  end

  def show
    @playlist = Playlist.find(params[:id])
    @song = Song.new
    respond_to do |format|
      format.json { render json: PlaylistSongBlueprint.render(@playlist.playlist_songs.order(:order)) }
      format.html
    end
  end

  def index 
    @playlists = current_user.playlists
  end
end
