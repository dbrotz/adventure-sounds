class ScenesController < ApplicationController
  layout 'game'
  helper AttachmentHelper
  before_action :authenticate_user!
  before_action :setup, except: [:new, :create]

  def index
  end

  def create
    @scene = Scene.new(scene_params)
    @scene.game_id = @game.id
    @scene.image.attach(params[:scene][:image])
    if @scene.save then
      redirect_to game_scenes_path(@game)
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def new
    @scene = Scene.new
  end

  def set
    @game.update!(current_scene: @scene)
    GameChannel.broadcast_to(@game, {description: @scene.description, image: url_for(@scene.image.variant(resize_to_limit: [800, 800])), audio: url_for(@scene.audio)})
    redirect_to action: :show
    #head :no_content
  end

  def update
    if @scene.update(scene_params) then
      redirect_to game_scenes_url(@game)
    else
      render :edit
    end
  end

  def destroy
    @scene.destroy
    redirect_to request.referrer
  end

  private 

  def scene_params
    params.require(:scene).permit(:name, :description, :category, :game_id, :audio, :image)
  end

  def setup
    @game = Game.find(params[:game_id])
    @scene = Scene.find(params[:id])
  end
end
