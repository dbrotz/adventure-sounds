class MusicController < ApplicationController
  before_action :authenticate_user!

  def index
    @music = Song.new
  end

  def create
    @music = Song.create(audio: params[:audio]) 

	current_user.audio.attach(params[:audio])
	redirect_to user_music_index_url(current_user)
  end

  def destroy
    current_user.audio.find(params[:id]).purge
	redirect_to user_music_index_url(current_user)
  end

  def music_params
    params.require(:music).permit(:audio)
  end
end
