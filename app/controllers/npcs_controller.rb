class NpcsController < CharactersController
  def create
    @game = params[:game_id]
    @character = Character.new(user: current_user, name: params[:character][:name])
    @character.game_id = @game
  end

  def new
    super
  @character.is_player = false
  end
end
