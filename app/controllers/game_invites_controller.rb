class GameInvitesController < ApplicationController
  def update
    if params[:game_invite][:status] == "accepted" then
      invite = current_user.game_invitations.find(params[:id])
      #FIXME: Check if there is more than one character with the same name, validate character isn't in a game
      invite.game.players << current_user
      invite.destroy
    end
  end
end
