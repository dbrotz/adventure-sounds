class GamesController < ApplicationController
  layout 'game'
  before_action :authenticate_user!
  helper AttachmentHelper
  protect_from_forgery except: [:set_theme]

  def show
     @game = Game.find(params[:id])
     @scene = @game.current_scene || Scene.new
	 @audio_url = @game.audio_url
  end

  def admin
    @game = Game.find(params[:game_id])
  end

  def music
    @game = Game.find(params[:game_id])
  end

  def create
    @game = Game.new
    name = params[:game][:name]
    if name.nil? then
      render "users/show"
    end
    if @game.update(name: name, owner: current_user) then
      params[:players].each do |p|
        GameInvitation.create!(game: @game, user: User.find(p))
      end
      redirect_to game_admin_url(@game)
    else
      render "users/show"
    end
  end

  def set_theme
    @game = Game.find(params[:game_id])
    if @game.jukebox then
      @game.jukebox.destroy
    end

    @game.jukebox = Jukebox.create!(playlist: params[:theme_id])
    GameChannel.broadcast_to(@game, {audio: @game.jukebox.audio_url})
    head :no_content
  end

  def game_params
    params.permit(:id)
  end
end
