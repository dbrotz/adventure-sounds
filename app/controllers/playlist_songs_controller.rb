class PlaylistSongsController < ApplicationController
  before_action :authenticate_user!

  def new
	  @playlist = Playlist.find(params[:playlist_id])
    @playlist_sound = PlaylistSong.new
  end

  def update
    @playlist = Playlist.find(params[:playlist_id])
    body = JSON.parse(request.body.read)
    
    @song = @playlist.playlist_songs.find(params[:id])
    @song.swap_order(@playlist.playlist_songs.find(body["other"]))
  end
end
