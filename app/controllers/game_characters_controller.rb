class GameCharactersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_game

  def set_game
    @game = Game.find(params[:game_id])
  end

  def destroy
    @character = Character.find(params[:id])
    @character.game_id = nil
    @character.save
    redirect_to request.referer
  end

  def create
    @character = current_user.characters.find_by_name(params[:name])
    if not @character.nil? then
      @character.update(game_id: @game.id)
    else
    end
    if request.referer then
      redirect_to request.referer
    else
      redirect_to @game
    end
  end

  def index
    @characters = current_user.characters.where(game: nil)
  end
end
