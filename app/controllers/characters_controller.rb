class CharactersController < ApplicationController
  before_action :authenticate_user!

  def show
    @character = Character.find(params[:id])
  end

  def index
    @characters = current_user.characters
  end

  def new
    @character = Character.new(user: current_user)
    @character.is_player = true
    @character.stats = [
      {"name":"strength","value":10}, 
      {"name":"dexterity","value":10}, 
      {"name":"constitution","value":10},
      {"name":"wisdom","value":10},
      {"name":"intelligence","value":10},
      {"name":"charisma","value":10}
    ]
  end

  def create
    @character = Character.new(character_params)
    @character.user = current_user
#    @character.stats = [
#      {"name":"strength","value":10}, 
#      {"name":"dexterity","value":10}, 
#      {"name":"constitution","value":10},
#      {"name":"wisdom","value":10},
#      {"name":"intelligence","value":10},
#      {"name":"charisma","value":10}
#    ]
#    i = 0
#    params[:character][:attributes].each do |k, v|
#      @character.stats[i]["value"] = v
#      i = i + 1
#    end
    if @character.save then
      redirect_to request.referrer if request.referrer
      redirect_to home_path
    else
      render :new
    end
  end

  def edit
    @character = Character.find(params[:id])
  end

  def update
    @character = Character.find(params[:id])
    i = 0
    params[:character][:attributes].each do |k, v|
      @character.stats[i]["value"] = v
      i = i + 1
    end
    if @character.save then
      redirect_to user_characters_url(current_user)
    else
      render :edit
    end
  end

  def destroy
    @character = Character.find(params[:id])
    @character.destroy
    redirect_to user_characters_url(current_user)
  end

  def character_params
    params.require(:character).permit(:name)
  end
end
