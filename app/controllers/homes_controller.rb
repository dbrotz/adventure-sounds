class HomesController < ApplicationController
  before_action :authenticate_user!
  protect_from_forgery except: [:accept_friend]

  def friends
    @game = Game.find(params[:game_id])
	uid = params[:friend_uid].strip
    friend = Friend.create!(owner: current_user, friend: User.where(email: uid).or(User.where(name: uid)).take)
	redirect_to game_admin_url(@game)
  end

  def accept_friend
    byebug
    friend = Friend.find(owner: current_user, friend_id: params[:id])
    friend.update(status: :accepted)
  end

  def show
  end
end
