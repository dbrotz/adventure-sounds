class GamePlaylistsController < PlaylistsController
  before_action :authenticate_user!

  def index
    @game = Game.where(owner_id: current_user.id).find(params[:game_id])
    @playlists = @game.playlists
  end
end
