class SongsController < ApplicationController
  before_action :authenticate_user!

  def create
    uploaded_file = params[:song][:audio]
    name = params[:song][:audio].original_filename
    filename = File.basename(name, File.extname(name))
    SongsHelper.encode(uploaded_file)
    @song = Song.create!(user: current_user, audio: uploaded_file, name: filename) 

    redirect_to user_playlists_url(current_user)
  end

  def new
    @song = Song.new
  end

  def song_params
    params.require(:song).permit(:audio)
  end
end
