module GameCable
  class Connection < ApplicationCable::Connection
    identified_by :id

	def connect
	  self.id = SecureRandom.uuid
	end

	def disconnect
	end
  end
end

