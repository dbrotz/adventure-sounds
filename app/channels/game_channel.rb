class GameChannel < ApplicationCable::Channel
  def subscribed
    stream_for Game.find(game_id)
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def set(data)
    game = Game.find(game_id)
    scene = game.scenes.find(data["scene_id"])
    game.update!(current_scene_id: data["scene_id"])
    Channel.broadcast game, description: scene.description, image: scene.image.url, audio: scene.audio.url      
  end
  
  private

  def game_id
    params.fetch("id")
  end
end
