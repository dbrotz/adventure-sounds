// Action Cable provides the framework to deal with WebSockets in Rails.
// You can generate new channels where WebSocket features live using the `rails generate channel` command.

import consumer from "../channels/consumer"

document.addEventListener("DOMContentLoaded", function(event) { 
  consumer.subscriptions.create({ channel: "GameChannel", id: document.getElementById("game_id").value }, {
    connected() {
    },
  
    disconnected() {
    },
  
    received(data) {
    }
  });
});

function changeScene(id) {
  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", "scenes/" + id + "/set", true);
  xhttp.send();
}

import "controllers"
