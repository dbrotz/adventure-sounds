// Action Cable provides the framework to deal with WebSockets in Rails.
// You can generate new channels where WebSocket features live using the `rails generate channel` command.

import consumer from "../channels/consumer"
import "controllers"

document.addEventListener("DOMContentLoaded", function(event) { 
  consumer.subscriptions.create({ channel: "GameChannel", id: document.getElementById("game_id").value }, {
    connected() {
    },
  
    disconnected() {
    },
  
    received(data) {
      if(data.description !== undefined) {
        document.getElementById("description").textContent = data.description;
      }
      if(data.image !== undefined) {
        document.getElementById("image").src = data.image;
      }
      if(data.audio !== undefined) {
        changeAudio(data.audio)
      }
    }
  });
  function changeAudio(sourceUrl) {
    var audio = document.getElementById("player");
    
    if(audio == null) return;
    document.getElementById("mp3_src").src = sourceUrl;
    audio.pause();
    audio.load();
    audio.oncanplaythrough = audio.play();
  }
});
