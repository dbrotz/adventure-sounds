import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "song", "name", "length" ]

  setSong(event) {
    var song = JSON.parse(event.currentTarget.getAttribute("song"));
    this.nameTarget.textContent = song.name;
    this.lengthTarget.textContent = "0:00"
  }
}
