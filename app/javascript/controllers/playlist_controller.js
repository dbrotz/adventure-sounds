import { Controller } from "stimulus"
import sortable from "html5sortable/dist/html5sortable.es.js"

export default class extends Controller {
  static values = { display: String, currPlaylist: Number }

  initialize() {
    console.log(this.currPlaylistValue)
    this.currentPlaylist = null
    this.sortableId = "#" + this.displayValue
    this.sortableTable = null
  }

  clearTable() {
    var oldBody = document.getElementById(this.displayValue)
    var newBody = document.createElement("tbody")

    newBody.id = this.displayValue
    oldBody.parentNode.replaceChild(newBody, oldBody)
    return newBody;
  }

  updateTableRow(table, song, num) {
    var tr = document.createElement("tr")
    var songName = document.createElement("th")
    var songNum = document.createElement("th")

    songName.innerHTML = song.name
    tr.setAttribute("data-song-id", song.id)
    songNum.innerHTML = num

    tr.appendChild(songNum)
    tr.appendChild(songName)
    table.appendChild(tr)
  }

  updateSort(e) {
    var originElement = sortableTable.children[e.detail.origin.elementIndex]
    var destinationElement = sortableTable.children[e.detail.destination.elementIndex]
    var options = {
       method: "PATCH",
       headers: {
         'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').content
       },
       body: JSON.stringify({other: originElement.dataset.songId})
    }

    fetch("playlists/" + playlistId + "/playlist_songs/" + destinationElement.dataset.songId, options)

    var children = e.detail.destination.container.children
    for(var i = 0; i < children.length; i++) { 
      children[i].children[0].innerHTML = i + 1
    }
  }

  show(event) {
    var playlistId = event.currentTarget.parentElement.parentElement.getAttribute("playlist_id");
    var newBody = this.clearTable()
    var num = 1

    newBody.id = this.displayValue
    fetch("playlists/" + playlistId + ".json")
    .then(response => response.json())
    .then(data => {
      data.forEach(song => {
        this.updateTableRow(newBody, song, num)
        num = num + 1
      })
      this.sortableTable = sortable(this.sortableId)
      var sortableTable = this.sortableTable[0]
      
      this.sortableTable[0].addEventListener('sortupdate', this.updateSort)
    })
  }

  start(event) {
    var id = event.currentTarget.dataset.playlistIdValue
    var options = {
      method: "POST"
    }

    if(this.currentPlaylist != null) {
      var row = document.getElementById("playlist_" + this.currentPlaylist) 

      if(this.currentPlaylist == id) {
        row.children[0].children[0].classList.replace("fa-pause", "fa-play")
        this.currentPlaylist = null
        this.clearTable()
        return;
      }
      row.children[0].children[0].classList.replace("fa-pause", "fa-play")
    }
    event.currentTarget.classList.replace("fa-play", "fa-pause")
    this.show(event)
    this.currentPlaylist = id
    fetch("set_theme/" + id, options)
  }
}

