import { Controller } from "stimulus"

export default class extends Controller {
  static values = { userId: Number }

  accept(event) {
    this.updateInvite(event.currentTarget, "accepted")
  }

  refuse(event) {
    this.updateInvite(event.currentTarget, "rejected")
  }

  updateInvite(elem, accepted) {
    var row = elem

    while(row.tagName != "TR") {
      row = row.parentNode
      if(row == null) {
        console.log("Cannot find row for " + elem)
        return
      }
    }
    row.parentNode.removeChild(row)
    var options = {
       method: "PATCH",
       headers: {
         'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').content
       },
       body: JSON.stringify({status: accepted})
    }

    fetch(this.userIdValue + "/invites/" + elem.dataset.id, options)
  }
}
