import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "scene" ]

  setScene(event) {
    var scene = JSON.parse(event.currentTarget.getAttribute("scene"));
    var options = {
       method: "POST",
       headers: {
         'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').content
       }
    }
    fetch("scenes/" + scene.id + "/set", options)
  }
}

