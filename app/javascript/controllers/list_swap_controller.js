import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "output" ]
  static values = { companion: String }

  connect() {
  }

  move(event) {
    document.getElementById(this.companionValue)
      .appendChild(event.currentTarget)
  }
}
