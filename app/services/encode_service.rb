class EncodeService
  attr_reader :input_filename

  class EncodeError < StandardError
  end


  def self.encode(input_filename)
    new(input_filename).encode
  end

  def initialize(input_filename)
    @input_filename = input_filename
  end

  def encode
    if encode_and_strip then
      output_filename
    else
      raise EncodeError
    end
  end

  def output_filename
    File.join(Rails.root, "tmp", File.basename((input_filename), File.extname(input_filename)) + ".mp3")
  end

private

  def encode_and_strip
   `ffmpeg -loglevel panic -y -i #{input_filename} -vn -ar 44100 -ac 2 -b:a 192k -map_metadata -1 #{output_filename}`
   return $?.success?
  end
end
