module SongsHelper

  # Encodes an UploadedFile and relaces the temporary file with a new file
  # that is correctly encoded
  def encode uploaded_file
    f = File.open(EncodeService.new(params[:song][:audio].tempfile.path).encode)
    uploaded_file.tempfile.close
    FileUtils.mv(f.path, uploaded_file.path)
    uploaded_file.tempfile.open
  end
end
