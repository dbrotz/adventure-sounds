module CharactersHelper
  def display_character_name(user, game)
    if game.owner != user then
      character = game.characters.find_by(user: user)

      if character.nil? then
        "No Character"
      else
        link_to character.name, user_character_url(character)
      end
    else
      "Game Master"
    end
  end
end
