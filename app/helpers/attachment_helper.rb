module AttachmentHelper
  def attachment_url(attachment)
    if attachment.attached? then
	  url_for(attachment)
	else
	 ""
    end
  end
end
