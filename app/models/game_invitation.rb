class GameInvitation < ApplicationRecord
  enum status: {
    pending: 0,
    accepted: 1,
    rejected: 2
  }
  belongs_to :game
  belongs_to :user
end
