class Jukebox < ApplicationRecord
  belongs_to :game
  belongs_to :playlist
  after_commit :perform_job
  after_destroy :destroy_mount

  def audio_url
    config = Rails.application.config_for(:icecast)

	  "http://#{config[:hostname]}:#{config[:port]}/#{mount_point}?type=.mp3"
  end

private
  def perform_job
    JukeboxJob.perform_later(self.id)
  end

  def mount_point
    id
  end

  def destroy_mount
    config = Rails.application.config_for(:icecast)
    uri = URI.parse("http://#{config[:hostname]}:#{config[:port]}/admin/killsource").encode_www_form("mount", "/#{mountpoint})")
	  req = Net::HTTP::Get.new(uri)
	  req.basic_auth config[:user], config[:pass]
	  response = Net::HTTP.start(uri.hostname, uri.port) do |http|
    http.request(req)
	end
  end
end
