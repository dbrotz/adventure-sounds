class Friend < ApplicationRecord
  enum status: [ :pending, :accepted ]

  belongs_to :owner, foreign_key: :owner_id, class_name: "User"
  belongs_to :friend, foreign_key: :friend_id, class_name: "User"

  after_create :set_pending

  validates :owner_id, uniqueness: { scope: :friend_id }
  def set_pending
    self.status = :pending
  end
end
