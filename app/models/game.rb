class Game < ActiveRecord::Base
  belongs_to :current_scene, foreign_key: :current_scene_id, class_name: "Scene", optional: true
  belongs_to :owner, class_name: "User"

  has_one :jukebox, dependent: :destroy

  has_many :scenes, dependent: :destroy
  has_many :characters, dependent: :destroy

  validates :name, presence: true

  has_many :game_playlists, dependent: :destroy
  has_many :playlists, through: :game_playlists
  has_many :game_invitations
  has_many :accepted_invites, -> { where(game: self, status: accepted) }, class_name: "GameInvitation"

 def players
   game_invitations.where(status: :accepted).map do |i| i.user end
 end

  def npcs
    characters.where(is_player: false)
  end

  def audio_url
     unless jukebox.nil? then
       jukebox.audio_url
      else
	   ""
     end
  end
end

