class PlaylistSong < ApplicationRecord
  belongs_to :playlist
  belongs_to :song
  before_save :set_order

  def set_order
    if self.order.nil? then
      self.order = playlist.songs.count + 1
    end
  end

  def swap_order(other_song)
    playlist = self.playlist
    song_count = playlist.playlist_songs.count

    return if order < 0 \
           or order >= song_count
    order = other_song.order
    PlaylistSong.transaction do
      other_song.update!(order: self.order)
      self.update!(order: order)
    end
  end
end
