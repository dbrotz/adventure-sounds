class Character < ActiveRecord::Base
  class Attribute
    attr_accessor :name
    attr_accessor :value
    attr_accessor :order

    def initialize(h)
      puts h.class
      @name = h["name"]
      @value = h["value"]
      @order = h["order"]
    end
  end

  belongs_to :user
  belongs_to :game, optional: true

  has_one_attached :sheet
  has_one_attached :portrait

  validates_presence_of :name

  has_many :scene_characters
  has_many :scenes, through: :scene_characters

  enum state: [ :friend, :neutral, :foe ]

  def attribute(name)
    puts(self.stats[name.to_s])
    Attribute.new(JSON.parse(self.stats[name.to_s]))  
  end
end
