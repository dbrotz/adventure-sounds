class Song < ApplicationRecord
  belongs_to :user
  has_one_attached :audio

  validates :audio, blob: { content_type: :audio }
end
