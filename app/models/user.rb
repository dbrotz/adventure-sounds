class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :games, foreign_key: "owner_id"
  has_many :friend_membership, foreign_key: :owner_id, class_name: "Friend"
  has_many :friends, through: :friend_membership
  has_many :friends_accepted, -> {where(friends: {status: Friend.statuses[:accepted]})}, through: :friend_membership, source: "owner", foreign_key: :owner_id, class_name: "User"
  has_many :friends_pending, -> {where(friends: {status: Friend.statuses[:pending]})}, through: :friend_membership, source: "owner", foreign_key: :owner_id, class_name: "User"

  has_many :characters, -> { where(is_player: true) }, dependent: :destroy
  has_many :playlists, dependent: :destroy
  has_many :songs, dependent: :destroy
  has_many :game_invitations, dependent: :destroy
  has_many_attached :audio
end
