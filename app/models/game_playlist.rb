class GamePlaylist < ApplicationRecord
  belongs_to :game
  belongs_to :playlist
end
