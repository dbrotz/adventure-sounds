class Scene < ActiveRecord::Base
  belongs_to :game

  validates :name, presence: true
  validates :category, presence: true

  enum category: [ :major, :minor, :extra ]

  has_one_attached :image
  has_one_attached :audio

  has_many :scene_characters
  has_many :characters, through: :scene_characters

  def npcs
    characters.where(is_player: false)
  end
end
