class EncodingFailureMailer < ErrorMailer

  def failure(file)
    @file = file
    super
  end
end
