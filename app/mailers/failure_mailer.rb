class EncodingFailureMailer < ApplicationMailer

  def failure
    mail(to: "drbrotz@protonmail.com", subject: "dnd error")
  end
end

